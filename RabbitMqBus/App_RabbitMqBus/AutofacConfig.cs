﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace App_RabbitMqBus
{
    public static class AutofacConfig
    {
        public static readonly IConfiguration Configuration =
            new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()) // Microsoft.Extensions.Configuration
                .AddJsonFile("appsettings.json", true, true).Build();               // Microsoft.Extensions.Configuration.Json
        public static IServiceProvider ServiceProvider;


        private static readonly IServiceCollection Services = new ServiceCollection();
        private static readonly ILoggerFactory Logger = LoggerFactory.Create(builder => { });               
        public static void Configure()
        {
            Logger.AddLog4Net(Configuration["Log4NetConfigFile"]);
            Services.AddSingleton<ILoggerFactory, LoggerFactory>();
            Services.Add(ServiceDescriptor.Describe(typeof(ILogger<>), typeof(Logger<>), ServiceLifetime.Scoped));
            ServiceProvider = Services.BuildServiceProvider();
        }        
    }
}
