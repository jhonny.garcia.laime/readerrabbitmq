﻿using App_RabbitMqBus.Bus.Messages;
using App_RabbitMqBus.Components.RabbitMq;

namespace App_RabbitMqBus.Bus
{
    public class BusSubscriber : ISubscriber<CuentaCreada>, ISubscriber<CuentaModificada>
    {
        public void ProcessMessage(SenderMessage<CuentaCreada> sender)
        {
            
        }
        public void ProcessMessage(SenderMessage<CuentaModificada> sender)
        {

        }
    }
}
