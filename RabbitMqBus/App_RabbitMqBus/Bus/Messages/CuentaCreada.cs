﻿using App_RabbitMqBus.Components.RabbitMq;

namespace App_RabbitMqBus.Bus.Messages
{
    public class CuentaCreada : IMessage
    {
        public string Id { get; set; }
        public string[] LoginNames { get; set; }

        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string IdentityDocumentType { get; set; }
        public string IdentityDocument { get; set; }
        public string AlumnoIdIntegracion { get; set; }
        public string TrabajadorIdIntegracion { get; set; }
    }
}
