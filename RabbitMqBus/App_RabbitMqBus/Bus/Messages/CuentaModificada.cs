﻿using App_RabbitMqBus.Components.RabbitMq;

namespace App_RabbitMqBus.Bus.Messages
{
    public class CuentaModificada : IMessage
    {
        public string Id { get; set; }
    }
}
