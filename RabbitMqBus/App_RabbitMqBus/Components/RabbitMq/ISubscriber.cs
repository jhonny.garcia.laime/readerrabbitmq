﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_RabbitMqBus.Components.RabbitMq
{
    public interface ISubscriber<T> where  T : class
    {
        void ProcessMessage(SenderMessage<T> sender);
    }
}
