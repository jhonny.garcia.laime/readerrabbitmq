﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_RabbitMqBus.Components.RabbitMq
{
    public class RabbitMqConfiguration
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string VirtualHost { get; set; }
        public string ReceivedAckQueueName { get; set; }
        public string ApplicationKey { get; set; }
        public string QueueNameConsoleApplication { get; set; }
    }
}
