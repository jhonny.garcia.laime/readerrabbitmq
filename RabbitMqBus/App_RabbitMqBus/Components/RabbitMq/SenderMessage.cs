﻿namespace App_RabbitMqBus.Components.RabbitMq
{
    public class SenderMessage<T> where T : class
    {
        public SenderMessage(string sender, string key, T message) 
        {
            this.Sender = sender;
            this.Key = key;
            this.Message = message;
        }
        public SenderMessage() { }
        public string Sender { get; set; }
        public string Key { get; set; }
        public T Message { get; set; }
    }
}
