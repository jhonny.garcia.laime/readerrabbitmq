﻿using App_RabbitMqBus.Components.RabbitMq;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Linq;
using System.Text;
using App_RabbitMqBus.Bus;
using System.Collections.Generic;
using System.Text.Json;

namespace App_RabbitMqBus
{
    internal class Program
    {
        private const string
            MESSAGE_KEY_HEADER_NAME = "key",
            MESSAGE_NAME_HEADER_NAME = "name",
            MESSAGE_APPLICATION_HEADER_NAME = "application",
            MESSAGE_ACTION_HEADER_NAME = "action";

        private const string
            ACTION_PROCESS = "process",
            ACTION_RETRY = "retry",
            ACTION_REGISTER = "REGISTER",
            ACTION_UPDATE = "UPDATE";

        private static RabbitMqConfiguration RabbitMqConfiguration;

        private static IConnection Connection;
        private static IModel Channel;
        static void Main(string[] args)
        {
            AutofacConfig.Configure();

            RabbitMqConfiguration = AutofacConfig.Configuration
                .GetSection("Bus").Get<RabbitMqConfiguration>();

            var factory = GetFactoryRabbitMq(RabbitMqConfiguration);
            Connection = factory.CreateConnection();
            Channel = Connection.CreateModel();

            var messages = typeof(Program).Assembly.GetTypes()
               .Where(t => t.GetInterfaces().Any(i =>
                    i.IsAssignableFrom(typeof(IMessage))))
               .Select(t => t.Name)
               .ToArray();

            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += Consumer_Received;

            Channel.QueueDeclare(RabbitMqConfiguration.QueueNameConsoleApplication, true, false, false, null);
            foreach (var message in messages)
            {
                Channel.ExchangeDeclare(message, ExchangeType.Direct, true);
                Channel.QueueBind(RabbitMqConfiguration.QueueNameConsoleApplication,
                        message, message, null);
            }
            Channel.BasicConsume(
                    RabbitMqConfiguration.QueueNameConsoleApplication, false, consumer);

            Console.ReadKey();
        }

        private static void Consumer_Received(object sender, BasicDeliverEventArgs eventArgs)
        {
            string key = null,
             senderApp = null,
             messageContent = null,
             messageName = null,
             action = null;

            object msgKeyHeaderBytes = null,
            msgSenderHeaderBytes = null,
            msgNameHeaderBytes = null,
            msgActionHeaderBytes = null;

            eventArgs.BasicProperties.Headers?.TryGetValue(MESSAGE_KEY_HEADER_NAME, out msgKeyHeaderBytes);
            eventArgs.BasicProperties.Headers?.TryGetValue(MESSAGE_APPLICATION_HEADER_NAME,out msgSenderHeaderBytes);
            eventArgs.BasicProperties.Headers?.TryGetValue(MESSAGE_NAME_HEADER_NAME, out msgNameHeaderBytes);

            try
            {
                msgKeyHeaderBytes = msgKeyHeaderBytes as byte[];
                if (msgKeyHeaderBytes != null)
                {
                    key = Encoding.UTF8.GetString((byte[])msgKeyHeaderBytes);
                }

                msgSenderHeaderBytes = msgSenderHeaderBytes as byte[];
                if (msgSenderHeaderBytes != null)
                {
                    senderApp = Encoding.UTF8.GetString((byte[])msgSenderHeaderBytes);
                }

                messageName = eventArgs.RoutingKey;
                msgNameHeaderBytes = msgNameHeaderBytes as byte[];
                if (msgNameHeaderBytes != null)
                {
                    messageName = Encoding.UTF8.GetString((byte[])msgNameHeaderBytes);
                }

                if (msgActionHeaderBytes != null)
                {
                    action = Encoding.UTF8.GetString((byte[])msgActionHeaderBytes);
                }

                var body = eventArgs.Body;
                messageContent = Encoding.UTF8.GetString(body.Span);

                var messageType = typeof(Program).Assembly.GetTypes()
                     .Where(t => t.GetInterfaces().Any(i =>
                      i.IsAssignableFrom(typeof(IMessage)))
                   && t.Name == messageName).FirstOrDefault();

                if (messageType == null)
                {
                    throw new Exception("La aplicación no maneja ningún " +
                        $"proceso para el mensaje {messageName}");
                }

                var messageInstance = JsonSerializer.Deserialize(messageContent, messageType);

                var instance = Activator.CreateInstance(typeof(BusSubscriber));

                var context = typeof(SenderMessage<>);
                var typeArgs = new[] { messageType };
                var contextGeneric = context.MakeGenericType(typeArgs);

                var contextObject = Activator.CreateInstance(contextGeneric,
                    senderApp, key, messageInstance);

                var methodInfo = instance.GetType().GetMethod(nameof(ISubscriber<IMessage>.ProcessMessage));
                methodInfo.Invoke(instance, new[] { contextObject });

                RegisterMessageAck(ACTION_REGISTER, key, messageName, messageContent, null,
                    action == ACTION_RETRY, senderApp);
            }
            catch(Exception ex)
            {
                RegisterMessageAck(ACTION_REGISTER, key, messageName, messageContent, ex,
                    action == ACTION_RETRY, senderApp);
            }
            finally
            {
                ((EventingBasicConsumer)sender).Model.BasicAck(eventArgs.DeliveryTag, false);
            }
        }

        static ConnectionFactory GetFactoryRabbitMq(RabbitMqConfiguration config)
        {
            var factory = new ConnectionFactory()
            {
                HostName = config.Host,
                UserName = config.Username,
                Password = config.Password,
                VirtualHost = config.VirtualHost
            };

            if(int.TryParse(config.Port, out var port))
                factory.Port = port;

            return factory;
        }
        static void RegisterMessageAck(string action, string key, string name, string content, Exception exception, bool retry, string sender)
        {
            action = retry ? ACTION_UPDATE : action;
            var payload = new Dictionary<string, object>
            {
                {"Action", action},
                {"DateTime", DateTime.UtcNow},
                {"Key", key},
                {"Name", name},
                {"Content", content },
                {"Application", RabbitMqConfiguration.ApplicationKey},
                {"AccountId", "AppConsole"},//user for default
                {"ExceptionMessage", FlattenException(exception)},
                {"Aborted", false}, //always false
                {"Sender", sender}
            };
            var serialezeJson = JsonSerializer.Serialize(payload);
            var contenido = Encoding.UTF8.GetBytes(serialezeJson);
            
            var factory = GetFactoryRabbitMq(RabbitMqConfiguration);
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.BasicPublish("", RabbitMqConfiguration.ReceivedAckQueueName, null, contenido);
            }
        }
        public static string FlattenException(Exception exception)
        {
            if (exception == null) return null;

            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine($"Ocurrió una Excepción del tipo: {exception.GetType().FullName}");
            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
    }
}
